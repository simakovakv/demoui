UI test framework
======================
 - разрабатывался и тестировался на linux (Ubuntu 18.04), Chrome v83.0.4103.61
 - версия Java 8
 - система сборки Maven 3.6.3(Wrapper)
 - тестовый каркас JUnit4
 - Selenium, Chrome WebDriver v 83.0.4103.39
 
======================

Команды

----запуск автотестов----

mvn clean test (./mvnw clean test)

----allure отчет----

allure serve target/allure-results

======================

Архитектура:
PageObject, PageFactory (реализация Selenium)

Pages:
MainPage - страница поиска google.ru
GooglePage - play.google.com
WikiPage - ru.wikipedia.org

Steps:
MainStep - набор действий с pages 
Environment property - src/test/resources

Tests:
src/test/java/UITests.java

//TODO

Logger (Console output)

BDD: Cucumber