import org.junit.*;
import steps.MainSteps;
import util.Init;

import java.io.IOException;

public class UITest {
    @Before
    public  void start() {
        Init.startUp();
    }

    @After
    public  void shutDown() {
        Init.shutDown();
    }

    @Test
    public void testGoogleImages() {
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
                .setInputRequest("ivi.ru")
                .chooseCategoryFromMenu("Картинки")
                .filterPicRequestResults("Инструменты","Размер;Большой")
                .checkItems("ivi.ru");
    }

    @Test
    public void testGoogleLinks(){
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
            .setInputRequest("ivi.ru");
        for (int i = 2; i <= 5; i++) {
            mainSteps.findGoogleLinkStep("play.google.com") //TODO not stable
                    .pageNext(i);
        }
            mainSteps.checkRateStep("play.google.com");

    }

    @Test
    public void testIVIwiki(){
        MainSteps mainSteps = new MainSteps();
        mainSteps.goToTheSource()
                .setInputRequest("ivi.ru");
        for (int i = 2; i <= 5; i++) {
            try {
                mainSteps.findGoogleLinkStep("ru.wikipedia.org");//TODO not stable
                break;
            }catch (AssertionError ex){
                mainSteps.pageNext(i);
            }
        }
        mainSteps.goToTheSourceByClick("ru.wikipedia.org");
        mainSteps.checkLink();
    }
}




