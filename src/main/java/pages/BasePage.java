package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import util.Init;

import java.util.NoSuchElementException;
import java.util.function.Function;

abstract class BasePage {
    WebDriver driver;
    BasePage(){
        driver = Init.getWebDriver();
        PageFactory.initElements(driver, this);
    }
    void click(WebElement element){
        scrollTo(element);
        element.click();
    }
    void click(String xpath){
        click(driver.findElement(By.xpath(xpath)));
    }

    void clickWithJS(WebElement element){
        scrollTo(element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

    }
    void scrollTo(WebElement element){
        waitOfPage();
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoViewIfNeeded(true);", element);
    }
    void waitOfElement(Function func){
        new WebDriverWait(driver, 10).until(func);
    }
    void waitOfPage(){
        new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));
    }

    void moveTo(WebElement element){
        new Actions(driver).moveToElement(element).build().perform();
    }

    boolean checkVisibility(WebElement element){
        try {
            waitOfElement(ExpectedConditions.visibilityOf(element));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    boolean checkEnabled(String xpath){
        return(driver.findElements(By.xpath(xpath)).size() != 0);
    }

    void selectByText(WebElement element, String text){
        new Select(element).selectByVisibleText(text);
    }

    void fillInputField(WebElement element, String text){
        element.clear();
        //stupidClear(element);
        element.sendKeys(text + "\n");
        //element.sendKeys(Keys.ENTER);
    }

    WebElement constructFromStringFormat(String xpath){
        waitOfElement(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        return driver.findElement(By.xpath(xpath));
    }

    void refreshPage(){
        driver.navigate().refresh();
        waitOfPage();
    }
    void stupidClear(WebElement element){
        Actions navigator = new Actions(driver);
        navigator.click(element)
                .sendKeys(Keys.END)
                .keyDown(Keys.SHIFT)
                .sendKeys(Keys.HOME)
                .keyUp(Keys.SHIFT)
                .sendKeys(Keys.BACK_SPACE)
                .perform();
    }

    public void stupidWait(){
        try {
            Thread.sleep(10000);
        } catch(InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }


}
