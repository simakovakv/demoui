package pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import util.Init;

public class GooglePage extends BasePage{
    @FindBy(css="//div[contains(@aria-label,'Rated') and @class='BHMmbe']")
    private WebElement ratedNum;

    public GooglePage checkGoogleRated(String rate) {
        PageFactory.initElements(Init.getWebDriver(), this);
        waitOfPage();
        Assert.assertEquals("Рейтинги не равны", ratedNum.getText(), rate);
        return this;
    }

}
