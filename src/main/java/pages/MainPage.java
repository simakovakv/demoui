package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class MainPage extends BasePage {
    @FindBy(xpath = "//input[@title='Поиск']")
    WebElement inputSearchBlock;

    @FindBy(className = "fxgdke")
    List<WebElement> links;

    @FindBy(xpath = "//div[@class='rc' and contains(., 'Рейтинг')]")
    List<WebElement> rateElement;

    @FindBy(xpath = "//div[@class='rc' and contains(., 'Рейтинг')]//ancestor::div[@class='s']//preceding-sibling::div[@class='r']")
    List<WebElement> linkToRatePage;

    String categoryMenuFormat = "//a[contains(., '%s')]";

    String subCategoryFormat = "//div[contains(text(), '%s')]";

    String optionFormat = "//span[contains(.,'%s')]";

    @FindBy(xpath = "//cite[contains(text(),'')]")
    List<WebElement> googleLink;

    String numPage = "//a[contains(@aria-label,'Page %s' )]";

    private String rateString;

    public String getRateString() {
        return this.rateString;
    }

    public MainPage setFilterOption(String[] value) {
        WebElement subCategory = driver.findElement(By.xpath(String.format(subCategoryFormat, value[0])));
        click(subCategory);
        WebElement option = driver.findElement(By.xpath(String.format(optionFormat, value[1])));
        click(option);
        return this;
    }

    public MainPage enterSearchRequest(String text) {
        fillInputField(inputSearchBlock, text + "\n");
        return this;
    }

    //TODO remove hardcode
    public MainPage checkItems(String text) {
        Assert.assertTrue("Элементы не найдены или их мньше 3", links
                .stream().filter(s -> s.getText().contains(text)).count() > 3);
        return this;
    }

    public GooglePage checkRate(String text) {
        String rateText = rateElement.get(0).getText();
        System.out.println("rateText" + rateText);
        //TODO regex Pattern/Matcher
        int index = rateText.lastIndexOf("Рейтинг: ");
        rateString = rateText.substring(index+9,index+12);
        System.out.println("rateString" + rateString);
        if(!rateString.isEmpty()) {
            click(linkToRatePage.get(0));
            waitOfPage();
            System.out.println("click to another page");
        }
        return new GooglePage();
    }
    public MainPage changePage(int count) {
        WebElement element = driver.findElement(
                By.xpath(String.format("//a[@aria-label=\'Page %s\']", count)));
        scrollTo(element);
        clickWithJS(element);
        return this;
    }

    public MainPage findGoogleLink (String search) throws AssertionError{
        //PageFactory.initElements(Init.getWebDriver(), this);
        String text = "";
        boolean flag = false;
        for (int j = 0; j < googleLink.size()-1; j++) {
            try{
                text = driver.findElements(By.xpath("//cite[contains(text(),'')]")).get(j).getText();
                System.out.println(text + " and " + search);
                if(text.contains(search)){
                    flag=true;
                    System.out.println(text.contains(search));
                    break;
                }
            }catch(StaleElementReferenceException ex){
                System.out.println("FUUUUCK" + ex.getMessage());
            }
        }
        Assert.assertTrue("no such link", flag);
        return this;
    }


    public MainPage chooseCategoryMenu (String categoryName){
        /*new WebDriverWait(driver, 30).until((ExpectedCondition<Boolean>) wd ->
                ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete"));*/
        WebElement element;
        try {
            element = constructFromStringFormat(String.format(categoryMenuFormat, categoryName));
        } catch (Exception ex) {
            try {
                element = constructFromStringFormat(String.format(subCategoryFormat, categoryName));
            } catch (Exception exception) {
                throw new RuntimeException(exception.getMessage());
            }
        }
        click(element);
        return this;
    }

    public WikiPage goToTheSourceByClick(String link){
        click(driver.findElement(By.xpath("//cite[contains(text(),'ru.wikipedia.org')]")));
        return new WikiPage();
    }
}







