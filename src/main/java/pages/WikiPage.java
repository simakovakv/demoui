package pages;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import util.Init;

public class WikiPage extends BasePage{
    @FindBy(xpath="//a[@href='https://www.ivi.ru' and ancestor::td[@class='plainlist']]")
    WebElement link;

    public WikiPage checkLinks() {
        PageFactory.initElements(Init.getWebDriver(), this);
        waitOfPage();
        Assert.assertTrue("URL is not displayed", link.isDisplayed());
        click(link);
        waitOfPage();
        driver.getTitle().equals("Смотреть фильмы онлайн бесплатно в хорошем качестве, кино и видео фильмы можно смотреть бесплатно без регистрации и смс. Не можете скачать новинки – смотрите фильмы в онлайн кинотеатре ivi.ru прямо сейчас!");
        return this;
    }
}
