package steps;

import io.qameta.allure.Step;
import pages.GooglePage;
import pages.MainPage;
import pages.WikiPage;

public class MainSteps {
    MainPage mainPage;
    GooglePage googlePage;
    WikiPage wikiPage;

    @Step("переход на google.com")
    public MainSteps goToTheSource() {
        mainPage = new MainPage();
        //mainPage.refreshPage();
        return this;
    }

    @Step("Ввели в строку поиска")
    public MainSteps setInputRequest(String text) {
        mainPage = new MainPage();
        mainPage.enterSearchRequest(text);
        return this;
    }

    @Step ("Выбор категории из меню")
    public MainSteps chooseCategoryFromMenu(String category){
        mainPage.chooseCategoryMenu(category);
        return this;
    }

    @Step ("Отфильтровали результат поиска")
    public MainSteps filterPicRequestResults(String option, String value){
        String[] values = value.split(";");
        mainPage.chooseCategoryMenu(option);
        mainPage.setFilterOption(values);
        return this;
    }

    @Step ("Поиск ссылки в результатах поиска")
    public MainSteps findGoogleLinkStep(String text) {
        mainPage.findGoogleLink(text);
        return this;
    }
    @Step ("сменить страницу")
    public MainSteps pageNext(int count) {
        System.out.println("Call page next -" + count);
        mainPage.changePage(count);
        return this;
    }

    @Step ("Проверка рейтинга ivi в google play")
    public MainSteps checkRateStep(String text) {
        System.out.println("call check rate");
        googlePage = mainPage.checkRate(text);
        googlePage.checkGoogleRated(mainPage.getRateString());
        System.out.println(mainPage.getRateString());
        return this;
    }
    @Step ("Переход на другой ресурс")
    public MainSteps goToTheSourceByClick(String link){
        mainPage.goToTheSourceByClick(link);
        return this;
    }

    @Step ("Проверка ссылки с wiki на ivi")
    public MainSteps checkLink(){
        WikiPage wikiPage = new WikiPage();
        wikiPage.checkLinks();
        return this;
    }

    public MainSteps checkItems(String text){
        mainPage.checkItems(text);
        return this;
    }


}
